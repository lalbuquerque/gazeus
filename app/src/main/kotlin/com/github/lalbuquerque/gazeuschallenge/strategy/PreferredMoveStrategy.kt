package com.github.lalbuquerque.gazeuschallenge.strategy

import com.github.lalbuquerque.gazeuschallenge.entity.MoveSource

class PreferredMoveStrategy : MachineMoveStrategy {

    private val preferredMoves = arrayOf(intArrayOf(1, 1), intArrayOf(0, 0), intArrayOf(0, 2),
            intArrayOf(2, 0), intArrayOf(2, 2), intArrayOf(0, 1),
            intArrayOf(1, 0), intArrayOf(1, 2), intArrayOf(2, 1))

    override fun calculateNextMove(currentCells: Array<IntArray>): IntArray {
        preferredMoves
                .asSequence()
                .filter { currentCells[it[0]][it[1]] == MoveSource.EMPTY.id }
                .forEach { return it }

        return IntArray(0)
    }
}