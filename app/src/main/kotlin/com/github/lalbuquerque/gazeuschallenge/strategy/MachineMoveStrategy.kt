package com.github.lalbuquerque.gazeuschallenge.strategy

interface MachineMoveStrategy {
    fun calculateNextMove(currentCells: Array<IntArray>): IntArray
}