package com.github.lalbuquerque.gazeuschallenge.entity

enum class GameStatus {
    ONGOING, DRAW, PLAYER_WIN, PLAYER_LOSE
}