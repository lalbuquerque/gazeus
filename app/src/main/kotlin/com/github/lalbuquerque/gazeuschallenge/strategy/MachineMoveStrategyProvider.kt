package com.github.lalbuquerque.gazeuschallenge.strategy

class MachineMoveStrategyProvider {

    companion object {
        val PREFERRED_MOVE = "PREFERRED_MOVE"
    }


    fun get(key: String): PreferredMoveStrategy {
        if (key == PREFERRED_MOVE) {
            return PreferredMoveStrategy()
        }

        // Default
        return PreferredMoveStrategy()
    }
}