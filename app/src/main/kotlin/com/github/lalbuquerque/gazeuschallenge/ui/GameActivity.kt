package com.github.lalbuquerque.gazeuschallenge.ui

import android.content.Intent
import android.os.Bundle
import android.os.Handler
import android.support.v7.app.AppCompatActivity
import android.view.View
import android.widget.Button
import android.widget.TextView
import android.widget.Toast
import com.github.lalbuquerque.gazeuschallenge.R
import com.github.lalbuquerque.gazeuschallenge.entity.CellDrawType
import com.github.lalbuquerque.gazeuschallenge.entity.GameStatus
import com.github.lalbuquerque.gazeuschallenge.entity.MoveSource
import com.github.lalbuquerque.gazeuschallenge.extension.toast
import com.github.lalbuquerque.gazeuschallenge.service.ResultHandlerService
import com.github.lalbuquerque.gazeuschallenge.strategy.MachineMoveStrategyProvider

class GameActivity : AppCompatActivity() {

    val cells = arrayOf(intArrayOf(0, 0, 0), intArrayOf(0, 0, 0), intArrayOf(0, 0, 0))
    val playerDrawType = CellDrawType.X
    val machineMoveStrategy = MachineMoveStrategyProvider().get(MachineMoveStrategyProvider.
            PREFERRED_MOVE)
    val resultHandlerService = ResultHandlerService()

    var moveSource = MoveSource.PLAYER
    var turnText: TextView? = null
    var machineTurn: Boolean = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_game)

        turnText = findViewById(R.id.turnText) as TextView?
    }

    fun onCellClick(view: View) {
        if (machineTurn) return

        var x = 0
        var y = 1

        val id = view.id
        val button = findViewById(id) as Button

        when (id) {
            R.id.btn1 -> {
                x = 0
                y = 0
            }
            R.id.btn2 -> {
                x = 0
                y = 1
            }
            R.id.btn3 -> {
                x = 0
                y = 2
            }
            R.id.btn4 -> {
                x = 1
                y = 0
            }
            R.id.btn5 -> {
                x = 1
                y = 1
            }
            R.id.btn6 -> {
                x = 1
                y = 2
            }
            R.id.btn7 -> {
                x = 2
                y = 0
            }
            R.id.btn8 -> {
                x = 2
                y = 1
            }
            R.id.btn9 -> {
                x = 2
                y = 2
            }
        }

        if (cells[x] [y] != MoveSource.EMPTY.id) return

        button.text = getPlayerDrawing(playerDrawType)
        cells[x] [y] = moveSource.id

        handleMove(MoveSource.PLAYER, cells)
    }

    private fun handleMove(source: MoveSource, cells: Array<IntArray>) {

        when (resultHandlerService.execute(cells)) {
            GameStatus.ONGOING -> {
                if (source == MoveSource.PLAYER) {
                    machineTurn = true
                    turnText?.text = "Machine's turn!"

                    Handler().postDelayed({
                        handleMachineMove(machineMoveStrategy.calculateNextMove(cells))
                    }, 2000)

                } else {
                    machineTurn = false
                    turnText?.text = "Your turn!"
                }
            }
            GameStatus.PLAYER_WIN -> {
                toast("PLAYER WINS!", Toast.LENGTH_LONG)
                finish()
            }
            GameStatus.PLAYER_LOSE -> {
                toast("PLAYER LOSES!", Toast.LENGTH_LONG)
                finish()
            }
            GameStatus.DRAW -> {
                toast("DRAW!", Toast.LENGTH_LONG)
                finish()
            }
        }
    }

    private fun handleMachineMove(move: IntArray) {
        val x = move[0]
        val y = move[1]

        cells[x] [y] = MoveSource.MACHINE.id

        when {
            x == 0 && y == 0 -> {
                (findViewById(R.id.btn1) as Button).text = getMachineDrawing(playerDrawType)
            }
            x == 0 && y == 1 -> {
                (findViewById(R.id.btn2) as Button).text = getMachineDrawing(playerDrawType)
            }
            x == 0 && y == 2 -> {
                (findViewById(R.id.btn3) as Button).text = getMachineDrawing(playerDrawType)
            }
            x == 1 && y == 0 -> {
                (findViewById(R.id.btn4) as Button).text = getMachineDrawing(playerDrawType)
            }
            x == 1 && y == 1 -> {
                (findViewById(R.id.btn5) as Button).text = getMachineDrawing(playerDrawType)
            }
            x == 1 && y == 2 -> {
                (findViewById(R.id.btn6) as Button).text = getMachineDrawing(playerDrawType)
            }
            x == 2 && y == 0 -> {
                (findViewById(R.id.btn7) as Button).text = getMachineDrawing(playerDrawType)
            }
            x == 2 && y == 1 -> {
                (findViewById(R.id.btn8) as Button).text = getMachineDrawing(playerDrawType)
            }
            x == 2 && y == 2 -> {
                (findViewById(R.id.btn9) as Button).text = getMachineDrawing(playerDrawType)
            }

        }

        handleMove(MoveSource.MACHINE, cells)
    }

    fun onNewGameButtonClick(v: View) {
        startActivity(Intent(GameActivity@this, GameActivity::class.java))
    }

    private fun getPlayerDrawing(playerDrawType: CellDrawType): String {
        return if (playerDrawType == CellDrawType.X) "X" else "O"
    }

    private fun getMachineDrawing(playerDrawType: CellDrawType): String {
        return if (playerDrawType == CellDrawType.X) "O" else "X"
    }
}
