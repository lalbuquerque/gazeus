package com.github.lalbuquerque.gazeuschallenge.entity

enum class MoveSource(val id: Int) {
    EMPTY(0),
    PLAYER(1),
    MACHINE(2)


}