package com.github.lalbuquerque.gazeuschallenge.ui

import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.view.View
import com.github.lalbuquerque.gazeuschallenge.R
import com.github.lalbuquerque.gazeuschallenge.ui.GameActivity

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
    }

    fun onNewGameClick(v: View) {
        startActivity(Intent(MainActivity@this, GameActivity::class.java))
    }
}
