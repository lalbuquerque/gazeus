package com.github.lalbuquerque.gazeuschallenge.service

import com.github.lalbuquerque.gazeuschallenge.entity.GameStatus
import com.github.lalbuquerque.gazeuschallenge.entity.MoveSource

class ResultHandlerService {

    val playerMove = MoveSource.PLAYER.id
    val machineMove = MoveSource.MACHINE.id

    fun execute(currentCells: Array<IntArray>): GameStatus {
        var empty = false

        for (i in 0..2) {
            (0..2)
                    .asSequence()
                    .filter { currentCells[i][it] === 0 }
                    .forEach { empty = true }
        }

        for (i in 0..2) {
            if (currentCells[i][0] === playerMove &&
                    currentCells[i][1] === playerMove &&
                        currentCells[i][2] === playerMove) {
                return GameStatus.PLAYER_WIN
            }
            if (currentCells[i][0] === machineMove &&
                    currentCells[i][1] === machineMove &&
                        currentCells[i][2] === machineMove) {
                return GameStatus.PLAYER_LOSE
            }
        }
        //check vertical lines
        for (i in 0..2) {
            if (currentCells[0][i] === playerMove &&
                    currentCells[1][i] === playerMove &&
                        currentCells[2][i] === playerMove) {
                return GameStatus.PLAYER_WIN

            }
            if (currentCells[0][i] === machineMove &&
                    currentCells[1][i] === machineMove &&
                        currentCells[2][i] === machineMove) {
                return GameStatus.PLAYER_LOSE

            }
        }
        //check diagonals
        if (currentCells[0][0] === playerMove &&
                currentCells[1][1] === playerMove &&
                    currentCells[2][2] === playerMove) {
            return GameStatus.PLAYER_WIN

        }
        if (currentCells[0][0] === machineMove &&
                currentCells[1][1] === machineMove &&
                    currentCells[2][2] === machineMove) {
            return GameStatus.PLAYER_LOSE
        }
        if (currentCells[0][2] === playerMove &&
                currentCells[1][1] === playerMove &&
                    currentCells[2][0] === playerMove) {
            return GameStatus.PLAYER_WIN

        }
        if (currentCells[0][2] === machineMove &&
                currentCells[1][1] === machineMove &&
                    currentCells[2][0] === machineMove) {
            return GameStatus.PLAYER_LOSE
        }

        if (!empty) return GameStatus.DRAW

        return GameStatus.ONGOING
    }
}