package com.github.lalbuquerque.gazeuschallenge.entity

enum class CellDrawType { X, O }